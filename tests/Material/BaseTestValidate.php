<?php

namespace W7\Tests\Material;

use Itwmw\Validation\Factory;
use PHPUnit\Framework\TestCase;
use W7\Validate\Support\Storage\ValidateConfig;

class BaseTestValidate extends TestCase
{
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        $factory = new Factory();
        ValidateConfig::instance()->setFactory($factory)
            ->setRulesPath('W7\\Tests\\Material\\Rules\\');

        parent::__construct($name, $data, $dataName);
    }
}
