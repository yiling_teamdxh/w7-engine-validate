<?php

namespace W7\Tests\Test;

use W7\Tests\Material\BaseTestValidate;
use W7\Tests\Material\Rules\Chs;
use W7\Tests\Material\Rules\Length;
use W7\Tests\Material\UserRulesManager;

class TestRuleScene extends BaseTestValidate
{
    public function testGetAllRule()
    {
        $this->assertCount(5, (new UserRulesManager())->getCheckRules());
    }

    public function testSceneIsLogin()
    {
        $userRule = new UserRulesManager();

        $needRules = [
            'user' => 'required|email',
            'pass' => 'required|lengthBetween:6,16'
        ];

        $this->assertEquals($needRules, $userRule->scene('login')->getInitialRules());
    }

    public function testCustomValidateScene()
    {
        $userRule = new UserRulesManager();
        $rules    = $userRule->scene('register')->getCheckRules();

        $this->assertCount(4, $rules);
        $this->assertArrayHasKey('remark', $rules);
        $this->assertFalse(in_array('alpha_dash', $rules['remark']));
        $haveChsRule = false;
        foreach ($rules['remark'] as $rule) {
            if ($rule instanceof Chs) {
                $haveChsRule = true;
                break;
            }
        }
        $this->assertTrue($haveChsRule);

        $rules = $userRule->scene('registerNeedCaptcha')->getCheckRules();
        $this->assertCount(5, $rules);
        $this->assertArrayHasKey('captcha', $rules);
    }

    public function testExtendsRule()
    {
        $userRule = new UserRulesManager();

        $rules = $userRule->scene('captcha')->getCheckRules();

        $this->assertArrayHasKey('captcha', $rules);
        $haveRequiredRule = $haveCustomRule = $haveExtendRule = $extendRuleName = false;
        foreach ($rules['captcha'] as $rule) {
            switch ($rule) {
                case 'required':
                    $haveRequiredRule = true;
                    break;
                case $rule instanceof Length:
                    $haveCustomRule = true;
                    break;
                case 'checkCaptcha':
                    $haveExtendRule = true;
                    $extendRuleName = $rule;
                    break;
            }
        }

        $this->assertTrue($haveRequiredRule);
        $this->assertTrue($haveCustomRule);
        $this->assertTrue($haveExtendRule);

        $messages = $userRule->getMessages();

        $this->assertEquals('验证码错误', $messages['captcha.' . $extendRuleName]);
    }

    /**
     * @test 测试当使用了不存在的场景名，结果是否取出全部规则
     */
    public function testGetRulesUsingNonExistentSceneName()
    {
        $v = new class extends UserRulesManager {
            protected $rule = [
              'user' => 'required',
              'pass' => 'required',
              'code' => 'required',
              'name' => 'required'
           ];

            protected $scene = [
              'login' => ['user', 'pass']
           ];
        };

        $rules = $v->scene('login')->getCheckRules();
        $this->assertSame(2, \count(array_intersect(array_keys($rules), ['user', 'pass'])));
        $rules = $v->scene('nonExistentSceneName')->getCheckRules();
        $this->assertSame(4, \count(array_intersect(array_keys($rules), ['user', 'pass', 'code', 'name'])));
    }
}
