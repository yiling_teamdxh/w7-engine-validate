<?php

namespace PHPSTORM_META{
    expectedArguments(\W7\Validate\Validate::addEvent(), 0, 'event', 'before', 'after');
    expectedArguments(\W7\Validate\Validate::handleEventCallback(), 1, 'before', 'after');
}
