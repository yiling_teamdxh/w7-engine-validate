<?php

namespace W7\Validate\Support\Rule;

use Itwmw\Validation\Support\Interfaces\Rule;

interface RuleInterface extends Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     */
    public function passes($attribute, $value): bool;

    /**
     * Get the validation error message.
     */
    public function message(): string;
}
