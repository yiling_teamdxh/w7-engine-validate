<?php

namespace W7\Validate\Support\Processor;

enum ProcessorExecCond implements ProcessorSupport
{
    /**
     * Process data only when it's null.
     */
    case WHEN_EMPTY;

    /**
     * Process data only when it's not null.
     */
    case WHEN_NOT_EMPTY;

    /**
     * Process data under any circumstances, including null values.
     *
     * This is the default behavior and usually does not need to be specified.
     */
    case ALWAYS;
}
