<?php

namespace W7\Validate\Support;

use Itwmw\Validation\Support\Arr;
use Itwmw\Validation\Support\Str;
use W7\Validate\Support\Concerns\MessageProviderInterface;

class MessageProvider implements MessageProviderInterface
{
    /**
     * The array of custom attribute names.
     */
    protected array $customAttributes = [];

    /**
     * The array of custom error messages.
     */
    protected array $messages = [];

    /**
     * Data for validate.
     */
    protected array $data = [];

    /**
     * Attribute Name.
     */
    protected string $attribute = '';

    /**
     * Indexing when validating array structures.
     */
    protected array $attributeIndex = [];

    protected string $rule = '';

    public function setMessages(array $messages): static
    {
        $this->messages = $messages;
        return $this;
    }

    public function setCustomAttributes(array $customAttributes): static
    {
        $this->customAttributes = $customAttributes;
        return $this;
    }

    public function setData(array $data): static
    {
        $this->data = $data;
        return $this;
    }

    public function getInitialMessage(string $key = null, string $rule = null): string|array
    {
        if (is_null($rule)) {
            if (is_null($key)) {
                return $this->messages;
            } else {
                return $this->messages['key'] ?? '';
            }
        }

        $keyString = Common::makeMessageName($key, $rule);
        if (isset($this->messages[$keyString])) {
            return $this->messages[$keyString];
        }

        // 如果错误消息是由数组形式来定义的
        if (!empty($this->messages[$key])) {
            $message = $this->messages[$key];
            if (is_array($message)) {
                return $message[$rule] ?? '';
            }
        }

        return '';
    }

    public function handleMessage(string $messages): string
    {
        // 验证器没有找到对应的错误信息
        if (str_starts_with($messages, 'validation.')) {
            // 尝试将规则名转为首字母大写的驼峰式去查询
            $rule    = Str::studly($this->rule);
            $message = $this->getInitialMessage($this->attribute, $rule);
            if (empty($message)) {
                // 尝试将规则名转为首字母小写的驼峰式去查询
                $rule    = Str::camel($this->rule);
                $message = $this->getInitialMessage($this->attribute, $rule);
            }

            if (!empty($message)) {
                $messages = $message;
            }
        }
        return $this->replacingFieldsInMessage($messages);
    }

    public function setAttribute(string $attribute): static
    {
        $this->attribute = $attribute;
        if (preg_match_all('/\.(\d+)(?=\.|$)/', $attribute, $matches)) {
            $this->attributeIndex = $matches[1];
        } else {
            $this->attributeIndex = [];
        }

        return $this;
    }

    public function setRule(string $rule): static
    {
        $this->rule = $rule;
        return $this;
    }

    public function getMessage(string $key, string $rule = null): ?string
    {
        $error = $this->getInitialMessage($key, $rule);
        return $this->replacingFieldsInMessage($error);
    }

    /**
     * Replacing fields in error messages.
     *
     * @return string|string[]
     */
    private function replacingFieldsInMessage(string $message): array|string
    {
        if (preg_match_all('/{(&+)(\d+)}/', $message, $matches) > 0) {
            foreach ($matches[0] as $index => $pregString) {
                $_index         = $matches[2][$index];
                $attributeIndex = $this->attributeIndex[$_index] ?? 0;
                if ('&&' == $matches[1][$index]) {
                    ++$attributeIndex;
                }

                $message = str_replace($pregString, $attributeIndex, $message);
            }
        }

        if (preg_match_all('/{:(.*?)}/', $message, $matches) > 0) {
            foreach ($matches[0] as $index => $pregString) {
                $_index = 0;
                $key    = $matches[1][$index];
                while ($i = strpos($key, '*')) {
                    $attributeIndex = $this->attributeIndex[$_index] ?? 0;
                    $key            = substr_replace($key, $attributeIndex, $i, strlen($attributeIndex));
                    ++$_index;
                }
                $message = str_replace($pregString, Arr::get($this->data, $key, ''), $message);
            }
        }

        if (preg_match_all('/{@(.*?)}/', $message, $matches) > 0) {
            foreach ($matches[0] as $index => $pregString) {
                $message = str_replace($pregString, $this->customAttributes[$matches[1][$index]] ?? '', $message);
            }
        }

        return $message;
    }
}
